import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { CarritoService } from 'src/app/services/carrito.service';
import { LoginService } from 'src/app/services/login.service';
import { filterProduct } from 'src/app/classes/general';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public dropdownList: any[] = []
  public dropdownSettings: IDropdownSettings= {};
  public filterProducts:any[] = [];
  public noProducts = true;
  public txtSearchProduct = "";
  public searchValue:any;

  selectedItems:any = []

  constructor( private productsService: ProductosService, 
               private carritoService: CarritoService,
                private _loginService : LoginService
              ) { }

  ngOnInit(): void {
    this.getAllProducts();
    this.getItemsSelect();
    this.dropdownSettings  = {
      selectAllText:"Select all",
      idField: "_id",
      textField:"nombre",
    }
    this.carritoService.carrito$.subscribe( resp => {
      if(resp){
        this.getItemsSelect();
      }
    })
  }

  getItemsSelect(){
    this.selectedItems = []
    var cart = this.getProducts()
    for (var i = 0; i < cart.length; i++) {
      this.selectedItems.push({
        _id: cart[i]._id,
        nombre: cart[i].nombre
      })
    }
  }

  getAllProducts(){
    this.productsService.getAllProducts()
      .subscribe( (data: any) => this.setProducts( data.productos ));
  }

  public setProducts ( respData : any[] ){
    this.dropdownList =  respData;
    // console.log( this.dropdownList );
  }

  // public onSelectAll( e: any) {
  //   console.log(e);
  // }

  // public onItemDeSelect( e: any ){
  //   console.log(e._id);
  //   this.deleteProduct( e._id );
  // }
  
  public onItemSelect( event: any) {
    this.productsService.getProductById( event._id )
    .subscribe(( data: any ) => {
      data.producto.cantidadProducto = 1;
      this.addProduct( data.producto )
    });
  }

  addProduct( data: any ){
    
    if( data.cantidadActual >= 0 ){ 
      var cart:any[];
      data.cantidadProducto = 1;
      cart = this.getProducts()
      if( cart.length == 0){
        cart.push(data);
        this.filterProducts = [];
      }else{
        let productsCart = cart.find((el) => el._id === data._id)
        if(productsCart === undefined){;
          cart.push(data);
        }else{
          alert('Ya existe este prodcuto');
        }
      }

      
    localStorage.setItem('carrito', JSON.stringify(cart))
    this.carritoService.carrito$.emit(true)
    }
    
    if( data.cantidadActual <= 0 ){
      alert( "No se puede agregar el producto")

    }
  }

  deleteProduct( id:any ){
    var cart:any[]
    cart = this.getProducts()
    cart.forEach((prod, index) => {
      if( prod._id == id ){
        cart.splice( index, 1)
      }
    })
    localStorage.setItem('carrito', JSON.stringify(cart))
    this.carritoService.carrito$.emit(true)
  }

  getProducts(){
    var productos: string[]
    const k = localStorage.getItem('carrito');
    return productos = k !== null ? JSON.parse(k) : [];
  }

  logout(){ 
    this._loginService.logout();
    window.location.reload();
    localStorage.removeItem('carrito');
    localStorage.removeItem('total');
    localStorage.removeItem('cede');
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  }
  
  
  buscarProducto(inputSearch: any){
    this.searchValue = inputSearch;
    this.txtSearchProduct = inputSearch.value.toUpperCase(); 
    if( this.txtSearchProduct.length >= 3 ){ 

      this.filterProducts =filterProduct( this.dropdownList, this.txtSearchProduct );

    }else if(this.txtSearchProduct.length <= 2 ){
      this.filterProducts = [];
     }
  }
}
