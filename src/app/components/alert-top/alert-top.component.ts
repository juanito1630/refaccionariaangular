import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert-top',
  templateUrl: './alert-top.component.html',
  styleUrls: ['./alert-top.component.css']
})
export class AlertTopComponent implements OnInit {
  
  @Input() message: string = "";
  
  constructor() { }
  
  ngOnInit(): void {
  }

}
