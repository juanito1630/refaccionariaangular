import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertTopComponent } from './alert-top.component';

describe('AlertTopComponent', () => {
  let component: AlertTopComponent;
  let fixture: ComponentFixture<AlertTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
