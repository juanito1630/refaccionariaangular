import { Component, OnInit } from '@angular/core';
import CarritoI from 'src/app/interfaces/carrito';
import { CarritoService } from 'src/app/services/carrito.service';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  public showAlert = false;
  allCartProducts:any[] = []
  showMessageData:boolean = true

  constructor( public productsService : ProductosService, private carritoService: CarritoService ) {}

  ngOnInit(): void {
    this.getAllItems();
    this.getCart()
    this.carritoService.carrito$.subscribe( resp => {
      if(resp){
        this.getAllItems()
        this.getCart()
      }
    })
  }

  getCart(){
    if(this.allCartProducts.length == 0){
      this.showMessageData = true
    }else{
      this.showMessageData = false
    }
  }

  getAllItems(){ 
    const k = localStorage.getItem('carrito');
    this.allCartProducts = k !== null ? JSON.parse(k) : [];
  }

}
