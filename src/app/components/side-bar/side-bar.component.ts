import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  public user:any;
  constructor() { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() { 
    const local = localStorage.getItem('user');
    this.user = local != null ? JSON.parse(local) : null;
    // console.log(this.user.role)
  }

}
