import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavComponent } from './nav/nav.component';
import { CarritoComponent } from './carrito/carrito.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { RegistroClienteComponent } from './registro-cliente/registro-cliente.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertComponent } from './alerts/alert/alert.component'
import { RouterModule } from '@angular/router';
import { VentasComponent } from './ventas/ventas.component';
import { RegistroProductoComponent } from './registro-producto/registro-producto.component';
import { NavClientesComponent } from './nav-clientes/nav-clientes.component';
import { AlertClientsComponent } from './alerts/alert-clients/alert-clients.component';
import { AlertTopComponent } from './alert-top/alert-top.component';
import { NavProductsComponent } from './products/nav-products/nav-products.component';
import { NavUsersComponent } from './nav-users/nav-users.component';
import { AlertDeleteUsersComponent } from './alerts/alert-delete-users/alert-delete-users.component';



@NgModule({
  declarations: [
    DashboardComponent,
    NavComponent,
    CarritoComponent,
    SideBarComponent,
    RegistroClienteComponent,
    AlertComponent,
    VentasComponent,
    RegistroProductoComponent,
    NavClientesComponent,
    AlertClientsComponent,
    AlertTopComponent,
    NavProductsComponent,
    NavUsersComponent,
    AlertDeleteUsersComponent,

  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgMultiSelectDropDownModule.forRoot(),
    ReactiveFormsModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    NavComponent,
    CarritoComponent,
    DashboardComponent,
    AlertComponent,
    VentasComponent,
    SideBarComponent,
    NavClientesComponent,
    AlertClientsComponent,
    AlertTopComponent,
    NavProductsComponent,
    NavUsersComponent,
    AlertDeleteUsersComponent
  ]
})
export class ComponentsModule { }
