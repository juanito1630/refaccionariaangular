import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertClientService } from 'src/app/services/alerts/alert-client.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClientsDataService } from 'src/app/services/clients-data.service';
import { ResponseDeleteClientsService } from 'src/app/services/response-delete-clients.service';
import { modulos } from '../../data/nav-clientes/nav-clientes'
@Component({
  selector: 'app-nav-clientes',
  templateUrl: './nav-clientes.component.html',
  styleUrls: ['./nav-clientes.component.css']
})
export class NavClientesComponent implements OnInit {

  public modules = modulos;

  public clientsWillDelete : string[] = [];
  public clientSelected: string = "";
  public showAlert = false;
  public showAlertTop = false;
  public btnDelete = false;
  public messageAlertTop = "";
  public clientWillUpdate:string[] = []

  constructor(
    private _dataClients : ClientsDataService,
    private _dataAlerts: AlertClientService,
    private _clienteService: ClientesService,
    private _reponseDeleteCliente : ResponseDeleteClientsService,
    private _router: Router,
  ) { }

  ngOnInit(): void {

    this.listenKeyPress();

    this._dataClients.clientsWillDelete$.subscribe( data => this.setClientsWillDelete( data ))

    this._dataClients.clientUpdate$.subscribe( data => this.setClientsUpdate(data))

    this._dataAlerts.responseAlert$.subscribe( data  => {
      
      if(data){ 
        this.showAlert = false;
        this.removeClientsDB();
      }else {
        this.showAlert = false;  
      }
    })
  }

  setClientsWillDelete( clients : any ) {
    this.clientsWillDelete = clients;
  }

  setClientsUpdate( clients: any){
    this.clientWillUpdate = clients
  }

  deleteClients() {
    
    if( this.clientsWillDelete.length > 0  ){   
      this.showAlert = true;
    }else {
      this.showAlertTop = true;
      this.messageAlertTop = "Selecciona un cliente";

      setTimeout(() => {
        this.showAlertTop = false;
        this.messageAlertTop = "";
      }, 2500)
    }
  }

  removeClientsDB(){ 

    this.btnDelete = true;
    this.clientsWillDelete.forEach(  (id) => {

      this._clienteService.deleteClientById( id )
      .subscribe( (data:any) => {

        if(data.ok == false){ 
          this.messageAlertTop = data.message;
        }
      });
    });

    this._reponseDeleteCliente.responseDeleteClient$.emit(true);
    this.clientsWillDelete = [];
    this.btnDelete = false;
    this.showAlertTop = false;

  }


  validatePayHaveClient() { 

      this._router.navigateByUrl(`/abonos`)
  }


  // esta funcion se encarga de escuchar las teclas para ejecutar los comandos 
  listenKeyPress() { 
    window.addEventListener('keydown', (event) => {
      
      switch(event.key) {
        case 'F2':
          this._router.navigateByUrl("clientes/registro/cliente");
          break;
        
        case 'F4':
          this.deleteClients()
          break;

        case 'F6':
          this.validatePayHaveClient()
          break;

          default:

      }


    });  
  }

  ngOnDestroy() {
    window.removeEventListener('keydown', () => {
    });
  }

}
