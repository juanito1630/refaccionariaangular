import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AlertClientService } from 'src/app/services/alerts/alert-client.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registro-cliente',
  templateUrl: './registro-cliente.component.html',
  styleUrls: ['./registro-cliente.component.css']
})
export class RegistroClienteComponent implements OnInit {

  emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  
  createFormGroup(){
    return new FormGroup({
      idCliente: new FormControl('', [Validators.required]),
      nombre: new FormControl('', [Validators.required, ]),
      telefono: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("^[0-9]*$") ]),
      correo: new FormControl('', [Validators.required, Validators.pattern(this.emailExpression) ]),
      calle: new FormControl('', [Validators.required, ]),
      codigoPostal: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern("^[0-9]*$")]),
      // colonia: new FormControl('', [Validators.required, ]),
      // municipio: new FormControl('', [Validators.required, ]),
      notas: new FormControl('', [Validators.required, ]),
      folio: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      limiteCredito: new FormControl('', [Validators.required, ]),
      // RFC: new FormControl('', [Validators.required, ]),
      // razonSocialFacturacion: new FormControl('', [Validators.required, ]),
      // correoFacturacion: new FormControl('', [Validators.required, Validators.pattern(this.emailExpression) ]),
      // calleFacturacion: new FormControl('', [Validators.required, ]),
      // coloniaFacturacion: new FormControl('', [Validators.required, ]),
      // municipioFacturacion: new FormControl('', [Validators.required, ]),
      // codigoPostalFacturacion: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern("^[0-9]*$") ]),
      // notasFacturacion: new FormControl('', [Validators.required, ])
    })
  }

  clientForm: FormGroup

  showAlertSuccess:boolean = false
  dateNow = ''
  showAlertError:boolean = false
  idUser:string = ''
  messageAlert:string = ""
  titleAlert:string = ""
  buttonName:string = "Registrar"

  constructor( private clientesService: ClientesService, private _showAlerts: AlertClientService, private _router: ActivatedRoute ) { 
    this.clientForm = this.createFormGroup()
  }

  ngOnInit(): void {
    this.dateNow = new Date().toLocaleDateString()
    this._showAlerts.showAlert$.subscribe(data => {
      this.showAlertError = false
    })
    this.idUser = this._router.snapshot.paramMap.get('id') || '';
    this.getClientById()
  }
  
  getClientById(){
    if(this.idUser){
      this.buttonName = "Actualizar"
      this.clientesService.getCliente(this.idUser).subscribe(( cliente: any) => {
        // console.log(cliente);
        this.clientForm.setValue({
          RFC: cliente.RFC,
          calle: cliente.calle,
          // calleFacturacion: cliente.calleFacturacion,
          codigoPostal: cliente.codigoPostal,
          // codigoPostalFacturacion: cliente.codigoPostalFacturacion,
          // colonia: cliente.colonia,
          // coloniaFacturacion: cliente.coloniaFacturacion,
          correo: cliente.correo,
          // correoFacturacion: cliente.correoFacturacion,
          folio: cliente.folio,
          idCliente: cliente.idCliente,
          limiteCredito: cliente.limiteCredito,
          // municipio: cliente.municipio,
          // municipioFacturacion: cliente.municipioFacturacion,
          nombre: cliente.nombre,
          notas: cliente.notas,
          // notasFacturacion: cliente.notasFacturacion,
          // razonSocialFacturacion: cliente.razonSocialFacturacion,
          // telefono: cliente.telefono,
        })
      })
    }
    
  }

  onResetForm(){
    this.clientForm.reset()
  }

  addClient(){
    
    this.clientForm.value.telefono = parseInt(this.clientForm.value.telefono)
    this.clientForm.value.folio = parseInt(this.clientForm.value.folio)
    this.clientForm.value.codigoPostal = parseInt(this.clientForm.value.codigoPostal)
    // this.clientForm.value.codigoPostalFacturacion = parseInt(this.clientForm.value.codigoPostalFacturacion)

    if(this.idUser){
      // console.log('Actualizar');
      this.clientForm.value._id = this.idUser
      if(this.clientForm.valid){
        this.clientesService.updateClient(this.clientForm.value).subscribe(client => {
          // console.log( `Cliente actualizado:` )
          // console.log( client )
          this.showAlertSuccess = true
          this.titleAlert = "¡Actualizado!"
          this.messageAlert = "El cliente se actualizo corectamente..."
        })
      }else{
        this.showAlertError = true
        // console.log('Error...');
      }
    }else{
      // console.log('Registrar');
      if(this.clientForm.valid){
        this.clientesService.addClient(this.clientForm.value).subscribe(client => {
          // console.log( `Cliente Registrado:` )
          // console.log( client )
          console.log( client, "resp cliente" )
          this.showAlertSuccess = true
          this.titleAlert = "¡Registrado!"
          this.messageAlert = "El cliente se registro correctamente..."
        })
      }else{
        this.showAlertError = true
        // console.log('Error...');
      }
    }
  }

  get idCliente(){ return this.clientForm.get('idCliente') }
  get nombre(){ return this.clientForm.get('nombre') }
  get telefono(){ return this.clientForm.get('telefono') }
  get correo(){ return this.clientForm.get('correo') }
  get calle(){ return this.clientForm.get('calle') }
  get codigoPostal(){ return this.clientForm.get('codigoPostal') }
  // get colonia(){ return this.clientForm.get('colonia') }
  // get municipio(){ return this.clientForm.get('municipio') }
  get notas(){ return this.clientForm.get('notas') }
  get folio(){ return this.clientForm.get('folio') }
  get limiteCredito(){ return this.clientForm.get('limiteCredito') }
  get RFC(){ return this.clientForm.get('RFC') }
  // get razonSocialFacturacion(){ return this.clientForm.get('razonSocialFacturacion') }
  // get correoFacturacion(){ return this.clientForm.get('correoFacturacion') }
  // get calleFacturacion(){ return this.clientForm.get('calleFacturacion') }
  // get coloniaFacturacion(){ return this.clientForm.get('coloniaFacturacion') }
  // get municipioFacturacion(){ return this.clientForm.get('municipioFacturacion') }
  // get codigoPostalFacturacion(){ return this.clientForm.get('codigoPostalFacturacion') }
  // get notasFacturacion(){ return this.clientForm.get('notasFacturacion') }

}
