import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AlertClientService } from 'src/app/services/alerts/alert-client.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() title: String = ''
  @Input() message: String = '';
  @Input() icon: boolean = true

  constructor(private _router: Router, private _showAlerts: AlertClientService) { }

  ngOnInit(): void {
  }

  hiddeAlert(){
    if(this.icon){
      this._router.navigateByUrl("/ventas/productos");
    }else{
      this._showAlerts.showAlert$.emit(false);
    }
  }

}
