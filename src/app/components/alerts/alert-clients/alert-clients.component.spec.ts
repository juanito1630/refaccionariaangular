import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertClientsComponent } from './alert-clients.component';

describe('AlertClientsComponent', () => {
  let component: AlertClientsComponent;
  let fixture: ComponentFixture<AlertClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertClientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
