import { Component, OnInit } from '@angular/core';
import { AlertClientService } from 'src/app/services/alerts/alert-client.service';

@Component({
  selector: 'app-alert-clients',
  templateUrl: './alert-clients.component.html',
  styleUrls: ['./alert-clients.component.css']
})
export class AlertClientsComponent implements OnInit {

  constructor(
    private alertService: AlertClientService
  ) { }

  ngOnInit(): void {
  }

  confirmDelete(){ 
    this.alertService.responseAlert$.emit(true);
  }

  cancelDelete() { 
    this.alertService.responseAlert$.emit(false);
  }
}
