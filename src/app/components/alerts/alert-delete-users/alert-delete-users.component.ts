import { Component, OnInit } from '@angular/core';
import { DeleteServiceDataService } from '../../../services/users/delete/delete-service-data.service'

@Component({
  selector: 'app-alert-delete-users',
  templateUrl: './alert-delete-users.component.html',
  styleUrls: ['./alert-delete-users.component.css']
})
export class AlertDeleteUsersComponent implements OnInit {

  constructor(
    private _userDeleteData : DeleteServiceDataService 
  ) { }

  ngOnInit(): void {
  }

  confirmDeleteUser() {
    this._userDeleteData.usersWillDeleteConfirm$.emit(true);
   }

   cancelDeleteUser(){
     this._userDeleteData.usersWillDeleteConfirm$.emit(false);
   }

}
