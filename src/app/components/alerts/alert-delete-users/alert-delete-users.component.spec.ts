import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertDeleteUsersComponent } from './alert-delete-users.component';

describe('AlertDeleteUsersComponent', () => {
  let component: AlertDeleteUsersComponent;
  let fixture: ComponentFixture<AlertDeleteUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertDeleteUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertDeleteUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
