import { Component, OnInit } from '@angular/core';
import { UsersDataService } from 'src/app/services/users-data.service';
import { DeleteServiceDataService } from 'src/app/services/users/delete/delete-service-data.service';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-nav-users',
  templateUrl: './nav-users.component.html',
  styleUrls: ['./nav-users.component.css']
})
export class NavUsersComponent implements OnInit {

  public userSeletedDelete: string[]=[];
  public showAlert  = false;
  constructor(
    private _userDataServices: UsersDataService,
    private _userService: UsersService,
    private _deleteServiceData : DeleteServiceDataService
  ) { }

  ngOnInit(): void {
    this._userDataServices.$listenNewUser.subscribe( (data:any) => this.userSeletedDelete = data);
    this._deleteServiceData.usersWillDeleteConfirm$.subscribe( (data: any) => {
      
      if(data) {
        this.deleteUserConfirm()
      }else {
        this.showAlert = false;
        this.userSeletedDelete = [];
      }
    });
  }

  // funcion que se encarga de borrar a los usuarios que se han seleccionado
  // se comuni por el subcribe del servicio
  deleteUser(){ 
    if( this.userSeletedDelete.length == 0){
      alert("No hay usuario seleccionado")
    }else {
      this.showAlert = true;
    }
  }
  
  deleteUserConfirm() { 
    this.showAlert = false;
    
    this.userSeletedDelete.forEach( (idUser:any) => {
      this._userService.deleteUser( idUser )
      .subscribe( (data:any) => {
        
        if(data.ok) {
          this._userDataServices.$listenUsersDeleted.emit(true);
        }
      });
    });
    // fin del foreach
   }

}
