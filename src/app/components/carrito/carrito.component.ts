import { Component, OnInit } from '@angular/core';
import CarritoI from 'src/app/interfaces/carrito';
import { Router } from '@angular/router';
import { CarritoService } from 'src/app/services/carrito.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  allCartProducts:any[] = []
  total = 0
  public btnDisabled = true;
  isDisabled:boolean = true;

  constructor( private router: Router, private carritoService: CarritoService ) { }


  ngOnInit(): void {
    // const total = localStorage.getItem('total');
    this.getProductsCart();
    this.calcTotal();
    this.carritoService.carrito$.subscribe( resp => {
      if( resp ){
        this.getProductsCart();
      }
    })
  }

  getProductsCart(){
    const k = localStorage.getItem('carrito');
    this.allCartProducts = k !== null ? JSON.parse(k) : [];
    this.calcTotal()
  }

  deleteItem( id: number ){
    var cart:any[]
    cart = this.getProducts()
    cart.forEach((prod, index) => {
      if( prod._id == id ){
        cart.splice( index, 1)
      }
    })
    localStorage.setItem('carrito', JSON.stringify(cart))
    this.calcTotal()
    this.getProductsCart()
    this.carritoService.carrito$.emit(true)
  }

  changeAmount(i:any, event:any){
    var cart = this.getProducts()
    if( event.target.value < 1){
      alert('Cantidad no permitida')
    }else{
      cart.forEach( (product:any) => {
        this.total = 0;
        if( i === product._id ){

          if( product.cantidadProducto < product.cantidad ){  
            alert('La cantidad es mayor al stock')
          }else {
            product.cantidadProducto = parseInt(event.target.value);
            localStorage.setItem('carrito', JSON.stringify(cart));
          }
        }
      });
      this.calcTotal();
      this.carritoService.carrito$.emit(true)
    }
  }

  calcTotal(){
    var cart = this.getProducts(); 
    this.total = 0
    for (var i = 0; i < cart.length; i++) {
      this.total += cart[i].cantidadProducto * cart[i].precioVenta;
      localStorage.setItem('total', JSON.stringify(this.total));
    }
    if( this.total <= 0 ){ 
      this.btnDisabled = true;
      localStorage.setItem('total', JSON.stringify(this.total));
    }else{
      this.btnDisabled = false
     }
   }

   getProducts(){
    var productos: string[]
    const k = localStorage.getItem('carrito');
    return productos = k !== null ? JSON.parse(k) : [];
  }

  inputDescuento( event:any, index:any ){
    if( event.target.checked){
      (<HTMLInputElement>document.getElementById(`${index}`)).disabled = false
    }else{
      (<HTMLInputElement>document.getElementById(`${index}`)).disabled = true
    }
  }

  changeRoute(){
    if( this.btnDisabled ){ 
      return;
    }else{
      this.router.navigateByUrl('/pagos');
    }
  }

}
