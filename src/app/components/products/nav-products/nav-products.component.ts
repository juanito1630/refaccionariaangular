import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductsServiceService } from 'src/app/services/products-service.service';

@Component({
  selector: 'app-nav-products',
  templateUrl: './nav-products.component.html',
  styleUrls: ['./nav-products.component.css']
})
export class NavProductsComponent implements OnInit {

  public products: string[] = [];

  constructor(
    private $productsDelete: ProductsServiceService,
    private _productsService: ProductosService,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.listenProductsSelected();
    this.listenKeysDowns();
  }
  
  listenProductsSelected() {
    this.$productsDelete.productsWillDeleteData$
    .subscribe( (data: any) => {
      console.log(data, "data")
      this.products = data;
 
      });
    
  }

  productsWillDelete() { 
    console.log( this.products , "this.productos")
    if( this.products.length == 0 ) { 
      alert('Sin productos seleccionados');
     }else {
      this.products.forEach( data => {
        console.log(data, "data")
        this._productsService.deleteAProduct( data )
        .subscribe( (data) => { console.log(data) });
      });
      this.$productsDelete.updateProducts$.emit(true);
     }
  }

  listenKeysDowns() {  
    window.addEventListener('keydown', (e) => {
      switch (e.key) {
        case 'F2':
          this._router.navigateByUrl("productos/registro/producto");
      }
    });
  }

}
