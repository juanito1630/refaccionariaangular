import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AlertClientService } from 'src/app/services/alerts/alert-client.service';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-registro-producto',
  templateUrl: './registro-producto.component.html',
  styleUrls: ['./registro-producto.component.css']
})
export class RegistroProductoComponent implements OnInit {

  createFormGroup(){
    return new FormGroup({
      idProducto: new FormControl('', [Validators.required]),
      descripcion: new FormControl('', [Validators.required]),
      seVendePor: new FormControl('', [Validators.required]),
      precioCosto: new FormControl('', [Validators.required]),
      precioVenta: new FormControl('', [Validators.required]),
      familia: new FormControl('', [Validators.required]),
      precioMayoreo: new FormControl('', [Validators.required]),
      cantidadMinima: new FormControl('', [Validators.required]),
      cantidadActual: new FormControl('', [Validators.required]),
      nombre: new FormControl('', [Validators.required]),
    })
  }

  productForm: FormGroup

  showAlertSuccess:boolean = false
  showAlertError:boolean = false
  dateNow = ''

  constructor( private productService: ProductosService, private _showAlerts: AlertClientService ) { 
    this.productForm = this.createFormGroup();
  }

  ngOnInit(): void {
    this.dateNow = new Date().toLocaleDateString()
    this._showAlerts.showAlert$.subscribe(data => {
      this.showAlertError = false
    })
  }

  onResetForm(){
    this.productForm.reset();
  }

  addProduct(){
    if( this.productForm.valid ){
      this.productForm.value.precioVenta = parseInt(this.productForm.value.precioVenta)
      this.productForm.value.precioCosto = parseInt(this.productForm.value.precioCosto)
      this.productForm.value.precioMayoreo = parseInt(this.productForm.value.precioMayoreo)
      this.productForm.value.cantidadMinima = parseInt(this.productForm.value.cantidadMinima)
      this.productForm.value.cantidadActual = parseInt(this.productForm.value.cantidadActual)

      this.productService.addProduct( this.productForm.value ).subscribe( product => {
        // console.log('Producto Registrado');
        this.showAlertSuccess = true
      })
      this.onResetForm();
    }else{
      this.showAlertError = true
      console.log('Error');
    }
  }

  get idProducto(){ return this.productForm.get('idProducto') }
  get descripcion() { return this.productForm.get('descripcion') }
  get seVendePor() { return this.productForm.get('seVendePor') }
  get precioCosto() { return this.productForm.get('precioCosto') }
  get precioVenta() { return this.productForm.get('precioVenta') }
  get familia() { return this.productForm.get('familia') }
  get precioMayoreo() { return this.productForm.get('precioMayoreo') }
  get cantidadMinima() { return this.productForm.get('cantidadMinima') }
  get cantidadActual() { return this.productForm.get('cantidadActual') }
  get nombre() { return this.productForm.get('nombre') }

}
