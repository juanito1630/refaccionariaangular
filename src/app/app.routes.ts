import  { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { LoginComponent } from './login/login/login.component';
import { PageComponent  } from './pages/page/page.component'


const appRoutes: Routes = [
  { 
    path: '',
    component: PageComponent,
    children: [
        { path: 'dashboard', component: DashboardComponent },
        { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    ]
  },
  { path: 'login', component:LoginComponent  },
  { path: '**', component: LoginComponent }
// Alguna ruta que no definida muestra el componente login
];


export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
