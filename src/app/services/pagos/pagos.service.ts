import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL } from '../../config/config'


@Injectable({
  providedIn: 'root'
})
export class PagosService {

  constructor(
    private _http: HttpClient
  ) { }


  // hace el post de las ventas, que no son de abonos
  addSells(venta: any){ 

    const uri = `${URL}/registro/venta`;
    return this._http.post(uri, venta)
  }


  // hace el post de un credito
  addCredit( body: any ){ 

    const uri = `${URL}/agregar/credito`;
    return this._http.post( uri, body );
  }

  // agrega el abono a un credito
  addPay( abono : any, id : string ){

    const uri =  `${URL}/agregar/abono/${id}`;
    return this._http.post(uri, abono);
  
  }

  //obtenemos todos los creditos
  getAllCredits() { 
    const uri = `${URL}/ver/creditos`;
    return this._http.get( uri );
   }

   // obtenemos el credito por el ID

   getCreditByID( id: string ) { 
    
    const uri =  `${URL}/ver/abonos/${id}`;
    return this._http.get( uri );
  }

}
