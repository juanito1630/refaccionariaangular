import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertClientService {

  responseAlert$ = new EventEmitter<Boolean>();

  showAlert$ = new EventEmitter<Boolean>();
  
  constructor() { }

}
