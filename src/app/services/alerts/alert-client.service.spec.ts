import { TestBed } from '@angular/core/testing';

import { AlertClientService } from './alert-client.service';

describe('AlertClientService', () => {
  let service: AlertClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlertClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
