import { TestBed } from '@angular/core/testing';

import { ResponseDeleteClientsService } from './response-delete-clients.service';

describe('ResponseDeleteClientsService', () => {
  let service: ResponseDeleteClientsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResponseDeleteClientsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
