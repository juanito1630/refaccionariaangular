import { Injectable, EventEmitter } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ProductsServiceService {

   productsWillDeleteData$ = new EventEmitter <any>();
   updateProducts$ = new EventEmitter<Boolean>();  
   
   constructor() { }

}
