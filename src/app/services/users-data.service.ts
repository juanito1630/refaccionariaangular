import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {

  constructor() { }
  // Se emite cada vez que hay algun usuario nuevo seleccionado para la eliminacion
   $listenNewUser = new EventEmitter<String[]>();
   // emite la alerta de que los usuarios fueron borrados correctamente
   $listenUsersDeleted = new EventEmitter<Boolean>();
   
}
