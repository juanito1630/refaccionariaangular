import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResponseDeleteClientsService {

  responseDeleteClient$ = new EventEmitter<Boolean>();
  constructor() { }
}
