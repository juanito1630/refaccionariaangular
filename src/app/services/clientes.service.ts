import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from '../config/config'
import { Clientes } from 'src/app/interfaces/clientes';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor( private _http: HttpClient ) { }
  
  public addClient( client: Clientes ){
    const url = `${URL}/registro/cliente`
    return this._http.post( url, client )
  }

  public getAllClients() { 
    const url =  `${URL}/ver/clientes`;
    return this._http.get( url);
  }

  public deleteClientById ( id: string ) { 
    const uri = `${URL}/eliminar/cliente/${id}`;
    return this._http.delete(uri);
  }

  public getCliente(id:string){
    const uri = `${URL}/cliente/${id}`
    return this._http.get(uri)
  }

  public updateClient(client:Clientes){
    const uri = `${URL}/actualizar/cliente`
    return this._http.post(uri, client)
  }

}
