import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientsDataService {

  clientsWillDelete$ = new EventEmitter<String[]>();
  clientUpdate$ = new EventEmitter<String[]>();

  constructor() { }
}
