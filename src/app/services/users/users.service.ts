import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from '../../config/config'

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private _http: HttpClient
  ) { }

  postANewProduct (body: any) {

    const uri = `${URL}/nuevo/usuario`;
    return this._http.post(uri, body);
  }

    getUsers() {
      const uri = `${URL}/obtener/usuarios`;
      return this._http.get(uri);
     }

     getUserById( id: string) { 
      
      const uri = `${URL}/ver/usuario/${id}`;
      return this._http.get(uri);

     }

    deleteUser(id: string) {

      const uri = `${URL}/eliminar/usuario/${id}`;
      return this._http.delete(uri);
    }

    updateUser( id: string, body:any) {

      const uri = `${URL}/actualizar/usuario/${id}`;
      return this._http.put( uri, body );

   }
}
