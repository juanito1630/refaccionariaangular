import { TestBed } from '@angular/core/testing';

import { DeleteServiceDataService } from './delete-service-data.service';

describe('DeleteServiceDataService', () => {
  let service: DeleteServiceDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteServiceDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
