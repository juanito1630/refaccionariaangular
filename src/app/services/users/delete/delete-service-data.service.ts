import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeleteServiceDataService {

  usersWillDeleteConfirm$ = new EventEmitter <boolean>();
  constructor() { }
}
