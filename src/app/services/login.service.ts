import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL} from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public token = "";
  public  user = {
    date: "",
    email: "",
    lastName:"", 
    name:"",
    password:"", 
    role:"",
    _id: ""
  };

  constructor(
    private _http: HttpClient
  ) { }


  login( body: any ){ 

    const uri = `${URL}/autenticar/usuario`;
    return this._http.post( uri, body );
  }

  logout(){

    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.token = "";
    this.user.name = "";
  }



  isLogged() { 

    if( this.token.length <= 3 || this.token == null  ) { 
      return true;
    
    }else {

      return false;
      
     }

   }

}
