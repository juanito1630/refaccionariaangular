import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from '../config/config'
import { Products } from 'src/app/interfaces/products';


@Injectable({
  providedIn: 'root'
})


export class ProductosService {

  constructor(
    private _http: HttpClient
  ) { }

  public getAllProducts() {
    const url = `${URL}/obtener/productos`;
    return this._http.get( url );
  }

  public getProductById(id:string){
    const idP = {
      idProducto: id
    }
    const url =  `${URL}/obtener/producto/por/id`;
    return this._http.post( url, idP )
  }

  public addProduct( product: Products ){
    const url = `${URL}/registo/producto`;
    return this._http.post( url, product);
  }

  public deleteAProduct( id: string) { 
    const uri =  `${URL}/eliminar/producto/${id}`;
    return this._http.delete( uri);
  }

  public updateProducto(id: string, body:any) { 
    const uri =  `${URL}/editar/producto/${id}`;
    return this._http.put( uri, body);
  }

}
