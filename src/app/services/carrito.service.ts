import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {
  
  carrito$ = new EventEmitter<Boolean>();

  constructor() { }
}
