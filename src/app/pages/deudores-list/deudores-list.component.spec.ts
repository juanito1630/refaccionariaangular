import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeudoresListComponent } from './deudores-list.component';

describe('DeudoresListComponent', () => {
  let component: DeudoresListComponent;
  let fixture: ComponentFixture<DeudoresListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeudoresListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeudoresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
