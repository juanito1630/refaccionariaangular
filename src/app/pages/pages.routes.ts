import {  RouterModule } from '@angular/router';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { DeudoresListComponent } from './deudores-list/deudores-list.component';
import { InventarioComponent } from './inventario/inventario.component';
import { RegistroClienteComponent } from '../components/registro-cliente/registro-cliente.component';
import { RegistroProductoComponent } from '../components/registro-producto/registro-producto.component';
import { PageComponent } from './page/page.component';
import { PagosComponent } from './pagos/pagos.component';
import { VentaProductosComponent } from './venta-productos/venta-productos.component';
import { ClientesListComponent } from './clientes/clientes-list/clientes-list.component';
import { AbonosComponent } from './deudores/abonos/abonos.component';
import { GetAllAbonosComponent } from './get-all-abonos/get-all-abonos.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { UsersComponent } from './admin/users/users.component';
import { NewUserComponent } from './admin/new-user/new-user.component';
import { UserEditComponent } from './admin/users/user-edit/user-edit.component';
import { EditProducComponent } from './inventario/edit-produc/edit-produc.component';



const pagesRoutes =  [
    {
        path: '',
        component: PageComponent,
        children: [
            { path: '', component: DashboardComponent},
            // Abonos
            { path: 'cuenta', component: GetAllAbonosComponent },
            // ventas
            { path: 'pagos', component: PagosComponent },
            { path: 'ventas/productos', component: VentaProductosComponent},
            
            // inventario
            { path: 'inventario', component: InventarioComponent},

            // deudores
            { path: 'deudores', component: DeudoresListComponent},

            // clientes
            { path: 'clientes/registro/cliente', component: RegistroClienteComponent },
            { path: 'clientes/list', component: ClientesListComponent}, 
            { path: 'clientes/abonos/:id', component: AbonosComponent },
            { path: 'clientes/actualizar/cliente/:id', component: RegistroClienteComponent },
            
            // productos
            { path: 'products/list', component: ProductsListComponent},
            { path: 'productos/registro/producto', component: RegistroProductoComponent },
            { path: 'productos/editar/producto/:id', component: EditProducComponent },

            // usuarios
            //TODO: AGREGAR EL GUARD A ESTAS RUTAS QUE SOLO EL ADMIN LAS PUEDA VER
            { path: 'usuarios', component: UsersComponent },
            { path: 'usuario/nuevo', component: NewUserComponent },
            { path: 'editar/usuario/:id', component: UserEditComponent },
            {  path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        ]
    }
]


export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );