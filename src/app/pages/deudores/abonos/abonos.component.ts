import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PagosService } from 'src/app/services/pagos/pagos.service';

@Component({
  selector: 'app-abonos',
  templateUrl: './abonos.component.html',
  styleUrls: ['./abonos.component.css']
})
export class AbonosComponent implements OnInit {

  public tabPage = 1;
  public btnPagos = true;
  public idAbono = "";
  public restoAbonosTotal = 0;
  public showAlertSuccess = false;
  public titleAlert = "";
  public messageAlert = "";
  public messageError = "";
  public showAlertError = false;

  public dataCredit = {
    total: 0,
    anticipo: 0,
    pagos:[],
    produto:[],

  };

  public totalAbonos = 0;
  public abono = {
    monto: 0,
    montoTarjeta : 0,
    metodoPago: "",
    vendedor: "",
    fecha: "",
    referencia:"",
    restante: 0,
    cliente: {
      _id:""
    }
  }
  
  constructor(
    private _pagosService:PagosService,
    private _routes: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {

    this.idAbono = this._routes.snapshot.paramMap.get('id') || '';
    this.methodsPayment();
    this.getCreditByIdService();
  }


  //esta funcion obtiene la data del credito por el id
  getCreditByIdService() { 
    this._pagosService.getCreditByID( this.idAbono )
    .subscribe( (data: any) =>  this.setDataCredit(data.credito))
  }

    // saca el monto restante de los abonos
   getRestante() { 
     this.totalAbonos = 0;
     
     if( this.dataCredit.pagos.length != 0 ) { 
       this.dataCredit.pagos.forEach( (data:any) => {
         

        // se evalua el tipo de abono que se genero

        if(data.abono.metodoPago === 'Efectivo') {
          this.totalAbonos += data.abono.monto;
        }else if( data.abono.metodoPago == 'Tarjeta'){
          
          this.totalAbonos += data.abono.montoTarjeta;

        }else if( data.abono.metodoPago == 'Mixto') {

          this.totalAbonos += data.abono.montoTarjeta + data.abono.monto;
        }

       });

       this.restoAbonosTotal = this.dataCredit.total -  this.totalAbonos;
      //  console.log( this.restoAbonosTotal );
      } 
  }

   setDataCredit(data: any) { 
    this.dataCredit = data;
    this.abono.cliente._id = data.cliente._id;
    this.restoAbonosTotal = data.restante || 0;
    this.getRestante();
    }

  validateFields() { 


    if( this.tabPage == 1  ){ 

      if( this.abono.monto == 0 ){ 
        alert("Agrega un monto");
        return false; 
      }
    }else if( this.tabPage == 2 ) { 
      
      if( this.abono.montoTarjeta == 0 ) { 
        alert("Agrega un monto");
        return false;
      }else if( this.abono.referencia.length < 17  ) { 
        
        alert("Agrega un tarjeta valida");
        return false;

      } 
    }else if(  this.tabPage == 3 ){ 
      if( this.abono.monto == 0 ){ 
        return false;
      }else if( this.abono.montoTarjeta == 0 ){ 
        alert("Agrega un tarjeta valida");
        return false;
      }
    }

    this.btnPagos = false;
    return true;

   }

  methodsPayment () { 
    let j;
    const user = localStorage.getItem('user');
    ( j = user != null ? JSON.parse( user) : null);
    this.abono.vendedor = j._id;  
  }


  addMethodPayPost() {

    if( this.tabPage == 1  ){
      this.abono.metodoPago = "Efectivo";
    }else if( this.tabPage == 2 ){ 

      this.abono.metodoPago = "Tarjeta";
    }else if( this.tabPage == 3 ) {

      this.abono.metodoPago = "Mixto";
    }

  }

  changeTab( tab: number) {
    this.tabPage = tab;
  }


  addPay() { 
    this.addMethodPayPost();
    // if( this.abono.monto > this.restoAbonosTotal   ){ 
    //   alert("El monto es mayor");

    // }else {

      if( !this.btnPagos){
        this.abono.restante = this.restoAbonosTotal - this.abono.monto;
        
        this.btnPagos = true;
        this._pagosService.addSells({ vendedor: this.abono.vendedor, total: this.abono.monto, metodoPago: this.abono.metodoPago ,  cliente:  this.abono.cliente._id, productos: ['abono']  })
        .subscribe( (data:any) => {
          if(data.ok){
            this.getCreditByIdService();
            this.abono.monto = 0;
            this.abono.montoTarjeta = 0;

          }
        })
        this._pagosService.addPay( this.abono, this.idAbono )
        .subscribe(  (data: any) => {
          if(data.ok){ 
            this.showAlertSuccess = true;
            this.messageAlert = "Se registró el abono";
            this.titleAlert = 'Exito!';
            setTimeout(()=> {
              this._router.navigateByUrl("/ventas/productos");
            }, 2500)
           }else  {

            this.showAlertError = true;
            this.messageError = data.message;

           }
        })
  
        // }

    }
  }

}