import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAllAbonosComponent } from './get-all-abonos.component';

describe('GetAllAbonosComponent', () => {
  let component: GetAllAbonosComponent;
  let fixture: ComponentFixture<GetAllAbonosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetAllAbonosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetAllAbonosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
