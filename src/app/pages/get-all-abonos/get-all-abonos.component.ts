import { Component, OnInit } from '@angular/core';
import { ClientesService } from 'src/app/services/clientes.service';
import { PagosService } from 'src/app/services/pagos/pagos.service';
import {NgxPaginationModule} from 'ngx-pagination';

@Component({
  selector: 'app-get-all-abonos',
  templateUrl: './get-all-abonos.component.html',
  styleUrls: ['./get-all-abonos.component.css']
})
export class GetAllAbonosComponent implements OnInit {

  public credits: any[]= [];
  public totalCreditos = 0;
  public totalConAnticipo = 0;
  public total = "";
  public p =0 ;
  constructor(
    private _pagosService: PagosService
  ) { }

  ngOnInit(): void {
    this.getCredits();
  }


  getCredits() { 
    this._pagosService.getAllCredits()
    .subscribe( (data:any) =>  {
      this.setCredits(data.creditos);
      this.total = data.creditos;
    });
  }


  setCredits(credits: any){ 
    this.credits = credits;
    this.totalCreditos = 0;
    // obtenemos el total de los creditos que se han dado
    if( this.credits.length > 0  ) {
      this.credits.forEach( (data:any) =>  this.totalCreditos += data.total);
     }  
  }

}
