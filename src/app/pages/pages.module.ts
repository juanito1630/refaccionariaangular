import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page/page.component';
import { ComponentsModule } from '../components/components.module';
import { PagosComponent } from './pagos/pagos.component';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { PAGES_ROUTES } from './pages.routes';
import { VentaProductosComponent } from './venta-productos/venta-productos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InventarioComponent } from './inventario/inventario.component';
import { DeudoresListComponent } from './deudores-list/deudores-list.component';
import { ClientesListComponent } from './clientes/clientes-list/clientes-list.component';
import { AbonosComponent } from './deudores/abonos/abonos.component';
import { GetAllAbonosComponent } from './get-all-abonos/get-all-abonos.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { UsersComponent } from './admin/users/users.component';
import { CajaComponent } from './admin/caja/caja.component';
import { DevoluicionesComponent } from './admin/devoluiciones/devoluiciones.component';
import { NewUserComponent } from './admin/new-user/new-user.component';
import { UserEditComponent } from './admin/users/user-edit/user-edit.component';
import { EditProducComponent } from './inventario/edit-produc/edit-produc.component';



@NgModule({
  declarations: [
    PageComponent,
    PagosComponent,
    VentaProductosComponent,
    InventarioComponent,
    DeudoresListComponent,
    ClientesListComponent,
    AbonosComponent,
    GetAllAbonosComponent,
    ProductsListComponent,
    UsersComponent,
    CajaComponent,
    DevoluicionesComponent,
    NewUserComponent,
    UserEditComponent,
    EditProducComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    PAGES_ROUTES,
    ReactiveFormsModule
  ]
})
export class PagesModule { }
