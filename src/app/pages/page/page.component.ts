import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {


  public usuario: any;

  constructor(
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.getUser();
  }


  getUser() { 
    this.usuario = localStorage.getItem('user') || null;
    // console.log( this.usuario );

    if(this.usuario == null) { 
      this._router.navigateByUrl("/login");
    }else {
      return;
    }
  }

}
