import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';


@Component({
  selector: 'app-edit-produc',
  templateUrl: './edit-produc.component.html',
  styleUrls: ['./edit-produc.component.css']
})
export class EditProducComponent implements OnInit {

  public showAlertSuccess = false;
  public showAlertError = false;
  public   dateNow = '';
  public id= "";
  public productoGet = {
    cantidadActual: 0,
    cantidadMinima: 0,
    createdAt: "",
    descripcion: "",
    familia: "",
    idProducto: "",
    nombre: "",
    precioCosto: 0,
    precioMayoreo: 0,
    precioVenta: 0,
    seVendePor: "",
  }


  constructor(
    private _prodcutSErvice: ProductosService,
    private activatedRoute: ActivatedRoute,
  ) { 
    // this.productForm = this.createForm();
  }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '' ;
    this.getProductById();
    this.dateNow = new Date().toLocaleDateString();
  }

  // public createForm () { 
  //   return new FormGroup({
  //     idProducto: new FormControl('',[]),
  //     descripcion: new FormControl('', []),
  //     seVendePor: new FormControl('', []),
  //     precioCosto: new FormControl('', []),
  //     precioVenta: new FormControl('', []),
  //     familia: new FormControl('', []),
  //     precioMayoreo: new FormControl('', []),
  //     cantidadMinima: new FormControl('', []),
  //     cantidadActual: new FormControl('', []),
  //     nombre: new FormControl('', []),
  //   });
  // }

  public getProductById(){ 
    this._prodcutSErvice.getProductById( this.id )
    .subscribe( (data:any)  => {
      if(data.ok) {
        this.productoGet = data.producto;
      }

    })
  }

  // productForm: FormGroup

  


  public updateProduct () {

    this._prodcutSErvice.updateProducto( this.id, this.productoGet )
    .subscribe( (data:any)  => {
      if(data.ok){
        setTimeout(() => {
          this.showAlertSuccess = true;
        }, 2500)
      }
    })
  }

}
