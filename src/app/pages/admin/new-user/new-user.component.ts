import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  public dataUser = {
    name: '',
    phone: '',
    email:'',
    password:'',
    sexo: '',
    role: '',
    modules: ''
  }

  public showAlertSuccess = false;
  public titleAlert = "";
  public messageAlert = "";
  public messageError = "";
  public showAlertError = false;
  
  constructor(
    private _usersService : UsersService
  ) { }

  ngOnInit(): void {
  }

  public validateFields () { 

    if( this.dataUser.name.length == 0 ) {
      alert('Ingresa el nombre');
      return false;
    }else if( this.dataUser.sexo.length == 0 ) {
      alert('Ingresa el género');
      return false;
    }else if (this.dataUser.email.length === 0) {
      alert('Ingresa el correo');
    return false;}
    else if(this.dataUser.password.length === 0){
      alert('Ingresa el género');
      return false;
    } else if(this.dataUser.role.length === 0 || this.dataUser.role == "Select"){ 
      alert('Selecciona un rol');
      return false;
    }

    return true;
  }


  public sendData() {
    
    if( this.validateFields() ) {
      this._usersService.postANewProduct( this.dataUser )
      .subscribe( (data:any) => {
        
        if(  data.ok ){

          this.showAlertSuccess = true;
          this.titleAlert = 'Usuario registrado'
          this.messageError = data.message;
        }else {
          this.showAlertError = true;
          this.messageError = data.message;

          setTimeout(() =>{
            this.showAlertError = false;
          }, 3000);

        }
      })
    }
  }

}
