import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  constructor(
    private _userService: UsersService,
    private _router: ActivatedRoute,
    private _route : Router
  ) { }

  public titleAlert = '';
  public messageAlert = '';
  public messageError = '';
  public showAlertSuccess= false;
  public showAlertError = false;

  public userEdit  = {
    date: "",
    email: "",
    name: "",
    phone: "",
    role: "",
    sexo: "",
    id: ""
  }

  public idUser = "";

  ngOnInit(): void {
    this.obtenerUsuario();
    this.idUser = this._router.snapshot.paramMap.get('id') || '';
    console.log(this.idUser);
  }

  obtenerUsuario() { 
    this._userService.getUserById('60ee53a75b33a605e8dc62ab')
    .subscribe( (data: any)  => this.setUserData(data) );
  }


  setUserData(data:any) {
    
    this.userEdit = data.data;
  }

  actualizarInfo() { 
    this._userService.updateUser( this.idUser, this.userEdit)
    .subscribe( (data:any) => {

      if( data.ok ) {
        this.showAlertSuccess = true;
        this.messageAlert = data.message;
        this.titleAlert = 'Success';
      }else {
        
        this.showAlertError = true;
        this.messageError = data.message;

          setTimeout(() => {
            this.showAlertError = false;
            this._route.navigateByUrl('/ventas/productos');
          },  2500)
      }
    })
  }

}
