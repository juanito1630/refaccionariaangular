import { Component, OnInit } from '@angular/core';
import { UsersDataService } from 'src/app/services/users-data.service';
import { DeleteServiceDataService } from 'src/app/services/users/delete/delete-service-data.service';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users: any[] = [];
  public userWillDelete: string[] = [];
  public userData = {
    _id:""
  }

  constructor(
    private _userService: UsersService,
    private _userDataService: UsersDataService,
    private _deleteServiceData : DeleteServiceDataService
  ) { }

  ngOnInit(): void {
    //obtenemos la iformacion de usuario por medio del localStorage
    this.getUserDataStorage();

    this.getAllUsers()
    this._deleteServiceData.usersWillDeleteConfirm$.subscribe( (data:any) => {
        
       if( !data){ 
          this.getAllUsers();
        }
    });
    
    this._userDataService.$listenUsersDeleted.subscribe( (response: Boolean) => {

      if( response ) {
        this.getAllUsers();
      }
    });

  }


  getUserDataStorage() { 
    const k = localStorage.getItem('user');
    this.userData = k !== null ? JSON.parse(k) : this.userData;
    // console.log(this.userData);
  }


  getAllUsers() {
     this._userService.getUsers()
    .subscribe( (data:any) => this.setUsers(data));
  }

  setUsers( data:any ){
    this.users = data;
     this.users = this.users.filter( (users) =>  users._id != this.userData._id )
  }


  agregarUsuario( event: any, id : string ){
    if( event.target.checked ) {
      this.userWillDelete.push(id);
      this._userDataService.$listenNewUser.emit( this.userWillDelete );
      
    }else {
      // cuando se hace uncheked se ejecuta esta parte del codigo la cual flitra los elementos del array 
    this.userWillDelete = this.userWillDelete.filter( (item) => item != id);
      // console.log( this.userWillDelete);
      this._userDataService.$listenNewUser.emit( this.userWillDelete );
    }
  }

}
