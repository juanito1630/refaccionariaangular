import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevoluicionesComponent } from './devoluiciones.component';

describe('DevoluicionesComponent', () => {
  let component: DevoluicionesComponent;
  let fixture: ComponentFixture<DevoluicionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevoluicionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevoluicionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
