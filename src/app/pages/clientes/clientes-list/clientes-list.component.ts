import { Component, OnInit } from '@angular/core';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClientsDataService } from 'src/app/services/clients-data.service';
import { ResponseDeleteClientsService } from 'src/app/services/response-delete-clients.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  styleUrls: ['./clientes-list.component.css']
})
export class ClientesListComponent implements OnInit {
  
  public clients:any[] = [];
  public deleteClients: string[] = [];
  public updateClientList:string[] = []
  responseDelete:Subscription | undefined;
  responseClients:Subscription | undefined;
  clientsMessage:boolean = true

  constructor(
    private _clientesService: ClientesService,
    private _dataClients : ClientsDataService,
    private _responseDeleteClient : ResponseDeleteClientsService,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.deleteClients = []
    this.updateClientList = []
    this.getAllclients();
    // this.getClients()
    this.responseDelete = this._responseDeleteClient.responseDeleteClient$.subscribe( data  => {
      console.log(data);
      if(data){ 
        this.getAllclients();
        // this.getClients()
      }
    })
  }

  getAllclients() {
    this._clientesService.getAllClients()
    .subscribe( (data: any) => {
      if(data.ok){
        this.setClients(data.clientes);
      }else {
        alert("Algo paso");
      }
    });
  }

  setClients(clients:any){ 
    this.clients = clients;
  }

  // getClients(){
  //   if(this.clients == []){
  //     this.clientsMessage = true
  //   }else{
  //     this.clientsMessage = false
  //   }
  // }

  addClient( e:any, id: string ){
    if(e.target.checked){
      this.deleteClients.push(id);
    }else{
      this.deleteClients = this.deleteClients.filter(m=>m!=id)
    }
    this._dataClients.clientsWillDelete$.emit(this.deleteClients);
    
  }

  navigateUpdateClient(id:string){
    this._router.navigate(['/clientes/actualizar/cliente', id ]);
  }

  ngOnDestroy(){
    this.responseDelete?.unsubscribe()
    this.responseClients?.unsubscribe()
  }

}
