import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Carrito from 'src/app/classes/carrito';
import CarritoI from 'src/app/interfaces/carrito';
import { CarritoService } from 'src/app/services/carrito.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { PagosService } from 'src/app/services/pagos/pagos.service';
import { printTicket} from '../../classes/general';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent implements OnInit {

  public carrito1:CarritoI = {
    items: [],
    total:0
  };

  public alertOk = false;
  public cambio = 0;
  public iva = 0;
  public totalRest = 0;
  public btnPagos = true;

  constructor(
    private _pagosService: PagosService,
    private _router: Router,
    private  _clientesService: ClientesService,
    private _carritoService: CarritoService
  ) { }

  public sells ={
    productos:[],
    total: 0,
    cliente: "",
    vendedor: "",
    monto:0,
    metodoPago:"",
    referencia:"",
    montoTarjeta: 0
  }

  public credits = {

    cliente: "",
    total: 0,
    productos: [],
    anticipo:0,
    vendedor: "",
    restante: 0
  
  }

  public clientes:any[] = [];

  // INICIALIZAMOS EL TAB QUE ES LA VISTA POR DEFECTO
  public tabPage = 1;

  ngOnInit(): void {
    // OBTENEMOS TODOS LOS PRODUCTOS
    this.getProductsStorage();
    this.getUser();
    this.calcIva();
    this.calcTotal();
    this._carritoService.carrito$.subscribe( data => { 
      if(data){
        this.getProductsStorage();

      }
    });
  }


  // FUNCION QUE CAMBIA EL TAB
  changeTab(num: number){
    this.tabPage = num;

    if( this.tabPage == 3 ){ 
      this._clientesService.getAllClients()
      .subscribe( (data: any) => {
          // console.log(data)
        if(data.ok){
          this.setClients( data.clientes );
        }else{
          alert( data.message );
        } 

      });
    }
  
  }

  setClients(clients : any){ 
    this.clientes = clients;
  }

  getUser(){
    // obtiene del storage el usuario para la venta
    let j;
    const user = localStorage.getItem('user');
    ( j = user != null ? JSON.parse( user) : null);
    
    this.sells.vendedor = j._id ;
  }

   // esta funcion setea el cliente a la compra de credito
  agregarCliente( cliente: any ) {
    this.credits.total =  this.sells.total
    this.credits.restante = this.credits.total - this.credits.anticipo;
    if( cliente.limiteCredito < this.credits.total ){ 
      alert("El credito es superior");
    }else {
      this.credits.cliente = cliente._id;
      this.btnPagos = false;

    }
  }
  
  // VALIDAMOS LOS METODOS DE PAGO
  validateMethodPay(){ 

    switch(this.tabPage){
      case 1: 
      this.sells.metodoPago = "Efectivo";
      break;

      case 2:
      this.sells.metodoPago = "Tarjeta";
      break;

      case 3:
      this.sells.metodoPago = "Plazos";
      break;


      case 4:
      this.sells.metodoPago = "Mixto";
      break;

      default: 
      this.sells.metodoPago = "Tarjeta";

    }


  }

  postASell() { 

    if( this.validateFields() ){

      this.btnPagos = true;
      this._pagosService.addSells(this.sells)
        .subscribe( (data:any) => {
          if(data.ok){

            this.alertOk = true;
            localStorage.removeItem('carrito');
            localStorage.removeItem('total');
            // CIERRA LA VENTANA DE ALERTA
            setTimeout(() => {
              this.alertOk = false;
              this._router.navigateByUrl('/ventas/productos');
            }, 3000);
            
          }else {
            
            alert(data.message);
            this.deleteCarrito();
            this._router.navigateByUrl("/ventas/productos");
            
          }


      
     }
     
     );

   }

  }

  deleteCarrito () { 
    localStorage.removeItem('carrito');
  }
  //esta funcion postea una nueva compra a credito
  postACredit () { 
    
    if( this.validateFields() ) { 

      this.btnPagos = true;

      this._pagosService.addCredit( this.credits )
      .subscribe( (data: any) => { 
  
        
        if(data.ok){
          alert("Credito registrado");
          this.deleteCarrito();
          this._router.navigateByUrl('/ventas/productos');
         }else {
           alert(data.message);
           this.deleteCarrito();
           this._router.navigateByUrl('/ventas/productos');
         }
      })
    }
   }


  // FUNCION QUE SE EJECUTA CON EL PAGO
  postSells(){ 

    this.validateMethodPay();
    
    if( this.tabPage != 3  ){ 
        this.postASell();
     }else {
       
      this.credits.productos = this.sells.productos;
      this.credits.total = this.sells.total;
      this.credits.vendedor = this.sells.vendedor;
      //  console.log( this.credits );
      this.postACredit();
     
    }

  }

  // VALIDAMOS LOS CAMPOS DE PAGO EN EFECTIVO
  validateFields(){
    
    if(  this.tabPage == 1  ){ 

      // VALIDACION DE MONTO EN EFECTIVO
      if( this.sells.monto == 0 ){
      
        alert('Agrega un monto invalido');
        return false;
      }

    }else if( this.tabPage == 2 ){

      if( this.sells.referencia.length == 0 ){ 
        alert('Agrega una referencia valida');
        return false;
      }

      if( this.sells.montoTarjeta == 0 ){ 

        alert('Un monto valido');
        return false;

      }

    }else if( this.tabPage == 3){ 

      if( this.credits.productos.length == 0){
      
        alert("No hay productos cargados");
        return false; 
      
      }

    }else if( this.tabPage === 4 ) { 
      
      if( this.sells.montoTarjeta == 0){
        alert("Ingresa un monto valido");
        return false;
       
      }else if( this.sells.monto === 0 ) { 

        alert("Ingresa un monto valido");
        return false;

        
       }
    }

    if(this.carrito1.total == 0 ){
      alert('Total invalido');
      return false;
    }

    this.calcTotal();
 

    return true;

  }


  calcTotal(){ 

    this.totalRest = this.carrito1.total;

    if(  this.tabPage == 1){ 

     this.totalRest =  this.carrito1.total - this.sells.monto;
    
    }else if( this.tabPage == 2 ){ 

      this.totalRest =  this.carrito1.total - this.sells.montoTarjeta;
    
    }else if( this.tabPage == 3 ) {

    }else if( this.tabPage == 4 ){ 

      const restaSum = this.sells.monto + this.sells.montoTarjeta;
      this.totalRest = this.carrito1.total - restaSum;
    }

    if(  this.totalRest == 0 ){
      this.btnPagos = false;
     }

  }

  calcIva() { 
    this.iva = 0;
    this.iva =   ( this.carrito1.total * 0.16 );
  }

// OBTENEMOS TODOS LOS PRODUCTOS DEL STORAGE 
  getProductsStorage(){ 

    const l = new Carrito();
    const p = l.getAllItems();
    
    this.carrito1.items = p;
    this.carrito1.total = localStorage.getItem('total');

    this.sells.productos = this.carrito1.items;
    this.sells.total = parseInt(this.carrito1.total);
  }

  printTickets(){ 
    if(this.sells.cliente == ""){
      alert("Ingresa el nombre")
    }else {
      printTicket(this.carrito1, this.sells.cliente, this.sells, this.sells.total)

    }
  }

}
