import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductsServiceService } from 'src/app/services/products-service.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  public productos: any[] = [];
  public productsWillDelete : string[] = []; 

  constructor(
    private _productsService: ProductosService, 
    private $productsService: ProductsServiceService,

  ) { }

  ngOnInit(): void {
    
    this.getAllProducts();
    this.$productsService.updateProducts$.subscribe( data  => {
      if( data) {
        this.getAllProducts();
      }
     }) 
  }

  getAllProducts() {
    this._productsService.getAllProducts()
    .subscribe( (data:any)  => this.setProdcust(data.productos) );
  }

  setProdcust( products: any ){
    this.productos = products; 
  }

  setProductWilldelete( event: any , id: string ){
    if( event.target.checked  ){
      
      this.productsWillDelete.push(id);
       this.$productsService.productsWillDeleteData$.emit( this.productsWillDelete );
     }else {
     this.productsWillDelete = this.productsWillDelete.filter( (item: any)  => item._id == id);
     }
  }

}
