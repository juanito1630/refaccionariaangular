export const modulos = [
    {
        name: 'Nuevo',
        controls: 'F1',
        redirectTo: "/clientes/abonos"
    },
    {
        name: 'Editar',
        controls: 'F2',
        redirectTo: "/clientes/abonos"
    },
    {
        name: 'Eliminar',
        controls: 'F3',
        redirectTo: "/clientes/abonos"
    },
    {
        name: 'Estado de cuenta',
        controls: '',
        redirectTo: "/clientes/abonos"
    }, 
    {
        name: 'Abonos',
        controls: 'F5',
        redirectTo: "/clientes/abonos"
    },
    {
        name: 'Reportes',
        controls: 'F6',
        redirectTo: "/clientes/abonos"
    },
    {
        name: 'Liquidar',
        controls: 'F7',
        redirectTo: "/clientes/abonos"
    }, 
    {
        name: 'Importar',
        controls: 'F12',
        redirectTo: "/clientes/abonos"
    }
];