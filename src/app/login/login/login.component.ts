import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public body = {
    email: "",
    password: "",
  }

  public btnBlockButtonLogin = true;
  
  constructor(
    private _loginService: LoginService,
    private _router: Router
  ) { }

  ngOnInit(): void {
  }


  validateFieldsLogin() { 
    
    if( this.body.email.length <= 2 ){
  
      alert("Ingresa el nombre valido");
      return false;
  
    } else if(this.body.password.length <= 2){ 
      
      alert("Ingresa la contraseña valida");
      return false;
    
    } 

      this.btnBlockButtonLogin = false;
      return true;

  
    
  }

  login() {
    this._loginService.logout();
    
    if( this.validateFieldsLogin()){ 

      this.btnBlockButtonLogin = true;
      this._loginService.login( this.body  )
      .subscribe( (data:any) => {
      
          if(data.ok) { 
            
            localStorage.setItem('user', JSON.stringify(data.user));
            localStorage.setItem('token', JSON.stringify(data.token));
            this._router.navigateByUrl("/ventas/productos");
        
          }else { 

            alert(data.message);
            this.body.email = "";
            this.body.password = "";
          }
      } );

    }
  }

}
