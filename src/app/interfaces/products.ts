export interface Products {
    idProducto: String,
    descripcion: String,
    seVendePor: String,
    precioVenta: Number,
    precioCosto: Number,
    familia: String,
    precioMayoreo: Number,
    cantidadMinima: Number,
    cantidadActual: Number,
}