export interface Clientes {
    idCliente: String,
    nombre: String,
    telefono: Number,
    correo: String,
    calle: String,
    codigoPostal: Number,
    colonia: String,
    municipio: String,
    notas: String,
    folio: Number,
    limiteCredito: String,
    RFC: String,
    razonSocialFacturacion: String,
    correoFacturacion: String,
    calleFacturacion: String,
    coloniaFacturacion: String,
    municipioFacturacion: String,
    codigoPostalFacturacion: Number,
    notasFacturacion: String,
}