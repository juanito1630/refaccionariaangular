
export default class Localstorage {
    
    
    constructor(){

    }

    setLocalStorage( name: string, data: any ){
        localStorage.setItem(name, data)
    }

    getLocalStorage(name:string){
        const l = localStorage.getItem(name);
        return l;        
    }

    deleteStorage(name:string){

        localStorage.removeItem(name);
    }


}