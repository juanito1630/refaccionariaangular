
import CarritoI from '../interfaces/carrito';


export default class Carrito {

    public carrito1:CarritoI = {
        items: [],
        total:0
    };
    
    constructor(){
        this.getAllItems();
    } 
    
    setItem( item:any ){
        
        // verifica que el array este vacio

        if( this.carrito1.items.length === 0  ){
        
            this.carrito1.items.push( item );
            localStorage.setItem('carrito', JSON.stringify(this.carrito1) );
        }else {
            let productExists = this.carrito1.items.indexOf( item._id );

            console.log( productExists )
            
            if( productExists === -1 ){                 
                this.carrito1.items.push( item );           
                localStorage.setItem('carrito', JSON.stringify(this.carrito1) );
            

            }else if( productExists !=  -1 )  {
                
                return

            }
        }
      
    }

    getAllItems(){  
        const k = localStorage.getItem('carrito');
        this.carrito1 = k !== null ? JSON.parse(k) : this.carrito1;
        return this.carrito1;
    }

    replaceProducts( item: any ) {

        this.carrito1.items.forEach( (elements:any, idx: number) => {
            this.carrito1.items.splice(idx, 1)
            this.carrito1.items.push( item );
            localStorage.setItem('carrito', JSON.stringify(this.carrito1) )
        } )

    }

}