export const filterProduct = (productos: any, busqueda: any) => {
    const pacientesFilter:any[] = [];
    productos.forEach((element:any) => {
    
        if( element.nombre.includes( busqueda )){
            pacientesFilter.push( element );
        }
    
    });
    // console.log(pacientesFilter);
    return pacientesFilter;
};


//fucnion que se encarga de imprimir tickets
import jsPDF from "jspdf";
import * as moment from "moment";

export const printTicket = (carrito: any, cliente:any, infoVenta:any, total = 0 ) => {

    let imgLogo = '../../assets/img/logo.png';

var positionYPDF = 10;
var IVaDEl16 = 0;
var hoy = new Date();
var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();      
// atendio = getDataStorage().nombre;

const dates =  new Date()
const doc: any = new jsPDF();
doc.setFontSize(8);
// REvisar estas funciones
doc.addImage(imgLogo, 'PNG', 5, 1.5, 100, 20);
doc.text( 10 , 25,`No ticket: 'folio'          Fecha: ${infoVenta.fecha = dates.getDate()} Hora: ${hora}`);
// doc.text(2, 20,`RFC: HGS111116J76                  Teléfono: 735-35-77-564`);
//  doc.text(2, 25,`Atendió: ${infoVenta.vendedor} `);
doc.text( 20, 30, `Gracias ${ cliente }` );
doc.text(2, 35, `------------------------------------------------------------------------------------------------------------------------------------------------------------------`)
doc.text(2,40,`           Producto                                                                              Costo                `);
doc.text(2, 45, `------------------------------------------------------------------------------------------------------------------------------------------------------------------`)

positionYPDF += 40
carrito.items.forEach( ( item:any) => {
    doc.text(3, positionYPDF,`${item.nombre}`);
    doc.text(80,  positionYPDF,`$ ${item.precioVenta}.00 MX` );
    doc.text(3, positionYPDF += 15, ``)
    positionYPDF += 5;
  
});


IVaDEl16 = ((total  * 16 ) / 100);
doc.text(15, positionYPDF+=20, `El total sin I.V.A es:                 $ ${(total - IVaDEl16).toFixed(2)} MX` );
doc.text( 15, positionYPDF+=5,  `El I.V.A es de:                       $ ${(IVaDEl16).toFixed(2)} MX`  );
doc.save();
// window.open(doc.output('bloburl'));
}
