export class Producto {

    
    constructor(){ 
        cantidadActual: 0;
        cantidadMinima: 0;
        descripcion: "";
        familia: "";
        fechaRegistro: "";
        idProducto: "";
        nombre: "";
        precioCosto: 0;
        precioMayoreo: 0;
        precioVenta: 0;
        seVendePor: "";
        status: "";
        _id: "";
    }
  }
  

  export interface ResponseProduct {
    message: string
    ok: boolean
    producto: Producto
  }